﻿using System;

namespace CoreLibrary.Messages
{
    public class FailedMessage : BaseMessage
    {
        public string Error { get; set; }

        public FailedMessage(Guid correlationId)
            : base(correlationId)
        {
        }
    }
}