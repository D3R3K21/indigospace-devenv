﻿using System;

namespace CoreLibrary.Messages
{
    public class BaseMessage
    {
        public Guid CorrelationId { get; set; }

        public BaseMessage(Guid correlationId)
        {
            CorrelationId = correlationId;
        }
    }
}