﻿using System;

namespace CoreLibrary.Messages
{
    public class CommandMessage : BaseMessage
    {
        public string Command { get; set; }

        public CommandMessage(Guid correlationId)
            : base(correlationId)
        {
        }
    }
}