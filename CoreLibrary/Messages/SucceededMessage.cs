﻿using System;

namespace CoreLibrary.Messages
{
    public class SucceededMessage : BaseMessage
    {
        public string Message { get; set; }

        public SucceededMessage(Guid correlationId)
            : base(correlationId)
        {
        }
    }
}