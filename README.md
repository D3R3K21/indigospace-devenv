# Setup Docker #
* https://docs.docker.com/windows/step_one/
* once complete, add dev.docker to your host file with the IP shown in docker

# Install core services (Mongo & RabbitMQ) #
* docker run -d -p 5672:5672 -p 15672:15672 --hostname rabbit-host --name rabbitmq rabbitmq:3-management
* docker run --name mongodb -d -p 27017:27017 mongo:2.6.11

# Build Image for Service #
* cd "/path to project docker file (in this git repo)"
* docker build -t publisher:0.0.1 .

# Run Container #
* docker run -it -d -p 8080:3579 --name publisher --link rabbitmq:rabbit --link mongodb:mongo publisher:0.0.1