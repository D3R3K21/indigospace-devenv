﻿using System;
using System.Threading.Tasks;
using CoreLibrary.Messages;
using MassTransit;
using MTPublisher.Consumers;
using Nancy;

namespace MTPublisher
{


    public class IndexModule : NancyModule
    {

        private static IServiceBus _bus;

        static IndexModule()
        {
            _bus = ServiceBusFactory.New(bus =>
            {
                bus.UseJsonSerializer();
                bus.SetPurgeOnStartup(false);
                bus.ReceiveFrom("rabbitmq://rabbit:5672/testQueue");
                bus.UseRabbitMq();
                bus.Subscribe(sub => sub.Consumer(() => new TestConsumer()));
            });
        }

        public IndexModule()
        {
            Get["/"] = parameters =>
            {
                var id = Guid.NewGuid();
                var command = new CommandMessage(id);
                var response = new Response();
                Task x = null;
                Task y = null;
                _bus.PublishRequestAsync(command, async configure =>
                {

                    x = configure.Handle<FailedMessage>((context, message) =>
                    {
                        response = Response.AsJson(new { message.Error });
                    });
                    y = configure.Handle<SucceededMessage>((context, message) =>
                    {
                        response = Response.AsJson(new { message.Message });
                    });

                });
                Task.WaitAny(x, y);

                return response;
            };
        }
    }
}