﻿using System;
using CoreLibrary.Messages;
using MassTransit;

namespace MTPublisher.Consumers
{
    public class TestConsumer : Consumes<CommandMessage>.Context
    {
        public void Consume(IConsumeContext<CommandMessage> ctx)
        {
            var message = ctx.Message;
            var rando = new Random().Next();
            if (rando%2 == 0)
            {
                ctx.Respond(new SucceededMessage(message.CorrelationId)
                {
                    Message = "Some Message Goes Here"
                });
            }
            else
            {
                 ctx.Respond(new FailedMessage(message.CorrelationId)
                {
                    Error = "Some Error Goes Here"
                });
            }
           
        



        }
    }
}