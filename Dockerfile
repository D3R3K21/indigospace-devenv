FROM mono:3.12
EXPOSE 3579


#mkdir /var/mono
RUN mkdir -p /var/mono/MTPublisher
ADD . /var/mono/MTPublisher


WORKDIR /var/mono/MTPublisher
# Build our project
RUN nuget restore MTPublisher.sln
RUN xbuild MTPublisher.sln

# Change to our artifact directory
WORKDIR /var/mono/MTPublisher/MTPublisher/bin/Debug

# Entry point should be mono binary
ENTRYPOINT mono MTPublisher.exe